import ru.stachek66.nlp.mystem.holding.Factory;
import ru.stachek66.nlp.mystem.holding.MyStem;
import ru.stachek66.nlp.mystem.holding.MyStemApplicationException;
import ru.stachek66.nlp.mystem.holding.Request;
import ru.stachek66.nlp.mystem.model.Info;
import scala.Option;
import scala.collection.JavaConversions;

import java.io.File;
import java.text.BreakIterator;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextAnalyzer {

    HashMap<String, Integer> sortedTokens;
    final static Logger logger = LoggerFactory.getLogger(TextAnalyzer.class);
    LinkedHashMap<String, Integer> sentences;
    LinkedHashMap<String, HashSet<String>> sentencesTokens;
    private String str = "";

    public String getResult()
    {
        return str;
    }

    TextAnalyzer()
    {
        sentencesTokens = new LinkedHashMap<String, HashSet<String>>();
        sentences = new LinkedHashMap<String, Integer>();
    }
    private static String fullText;



    private final static MyStem mystemAnalyzer =
            new Factory("-ig --format json")
                    .newMyStem("3.0", Option.<File>empty()).get();


    public void referText(String fullText, int compressionRatio) throws MyStemApplicationException {
        this.fullText = fullText;


        /**делю текст на предложения*/
        splitSentences();

        /**получаю общую мапу токенов + для каждого предложения список токенов*/
        sortedTokens = getTokens();

        /**сортируем токены по встречаемости и не учитываем вес = 1*/
        sortedTokens = sortByValues(sortedTokens, 2);

        /**определяем вес предложений*/
        setWeigtsToSentences();

        /**ранжируем предложения по весу*/
        HashMap<String, Integer> sortedSentences = sortByValues(sentences, 0);

        /**удаляем предложения (процент сжатия)*/
        cutSentences(sortedSentences, compressionRatio);

        nullSentences();
        /**определяем порядок предложений*/
        orderSentences(sortedSentences);

        /**ранжируем предложения по порядку*/
       // sentences = sortByValues(sentences,0);

        /**собираем строку*/
        appendStr();
    }

    private boolean appendStr() {
        return true;
    }

    private void nullSentences() {
        int nOrder = 0;
        for(Iterator<Map.Entry<String, Integer>> it = sentences.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Integer> entry = it.next();
            sentences.put(entry.getKey(), -1);
        }
    }

    private void orderSentences(HashMap<String, Integer> sortedSentences) {

        for (Map.Entry<String, Integer> entry : sortedSentences.entrySet())
        {
            if (sentences.containsKey(entry.getKey()))
                sentences.put(entry.getKey(), 1);
        }

        int Count = 0;
        for (Map.Entry<String, Integer> entry : sentences.entrySet())
        {
            if (entry.getValue() == 1) {
                str += entry.getKey().toString();
                Count++;
            }
        }
    }

    private HashMap<String, Integer> getTokens() throws MyStemApplicationException {
        HashMap<String, Integer> unsortedTokens = new HashMap<String, Integer>();

        Iterable<Info> result;
        for (Map.Entry<String, Integer> entry : sentences.entrySet()) {

            HashSet<String> set = new HashSet<String>();
            result = getAllTokens(entry.getKey());

            for (final Info info : result) {
                if (!(info.rawResponse().contains("\"gr\":\"PR") ||         //предлог
                        info.rawResponse().contains("\"gr\":\"CONJ") ||     //союз
                        info.rawResponse().contains("\"gr\":\"SPRO") ||     //местоимение
                        info.rawResponse().contains("\"gr\":\"INTJ")||      //междометие
                        info.rawResponse().contains("\"gr\":\"PART")) &&    //частица
                        (!info.rawResponse().contains("parenth")))
                {
                    try
                    {
                        logger.info(info.lex().get());
                        String key = info.lex().get();

                        /**1) добавили в общую мапу токенов для дальнейшего ранжирования*/
                        if (!unsortedTokens.containsKey(key))
                            unsortedTokens.put(key, 1); //в начальной форме
                        else {
                            int n = unsortedTokens.get(key);
                            unsortedTokens.put(key, n + 1);
                        }

                        /**2) добавили в мапу предложение - список токенов в нем*/
                        set.add(key);

                    }
                    catch(Exception e) {
                    }
                }
            }

            sentencesTokens.put(entry.getKey() , set);
        }
        return unsortedTokens;
    }

    private Iterable<Info> getAllTokens(String fullText) throws MyStemApplicationException {
        this.fullText = fullText;
        return JavaConversions.asJavaIterable(
                mystemAnalyzer
                        .analyze(Request.apply(fullText))
                        .info()
                        .toIterable());
    }

    private static LinkedHashMap<String, Integer> sortByValues(HashMap map, int nThreshold) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        LinkedHashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            if (Integer.parseInt(entry.getValue().toString()) >= nThreshold)
                sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }

    private void splitSentences() throws MyStemApplicationException {


        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
        String source = fullText;
        iterator.setText(source);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            sentences.put(source.substring(start,end), 0);
            logger.debug(source.substring(start,end));
        }

    }

    private void setWeigtsToSentences() throws MyStemApplicationException {
        for (Map.Entry<String, HashSet<String>> entry : sentencesTokens.entrySet()) {

            for (String tok: entry.getValue())
            {
                if (sortedTokens.containsKey(tok))
                {
                    int weight = sentences.get(entry.getKey());
                    Integer curWeight = Integer.valueOf(sortedTokens.get(tok).toString());
                    sentences.put(entry.getKey(), curWeight + weight);
                }
            }
        }

        /*Iterable<Info> result;
        for (String key : sentencesTokens.keySet()) {
            HashSet<String> list;
            list = sentencesTokens.get(key);
            for (String str: list) {
                if (sortedTokens.containsKey(str)) {
                    int weight = sentences.get(key);

                    Integer curWeight = Integer.valueOf(sortedTokens.get(str).toString());
                    sentences.put(key,weight+curWeight);
                }
            }
        }*/
        }

    private void cutSentences(HashMap<String, Integer> sortedSentences, int compressionRatio) throws MyStemApplicationException {

        int nSent = sortedSentences.size();
        float x = nSent * (100-compressionRatio)/100;

        int nCount = 0;
        for(Iterator<Map.Entry<String, Integer>> it = sortedSentences.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Integer> entry = it.next();
            if(nCount >= x) {
               it.remove();
            }
            nCount++;
        }
    }
}

