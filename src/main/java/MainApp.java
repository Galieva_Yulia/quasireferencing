import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import static javafx.application.Application.launch;

public class MainApp extends Application{
    public static void main(String[] args) throws Exception {
        launch(args);
    }

    public void start(Stage stage) throws Exception {

        String fxmlFile = "view/MainApp.fxml";
        FXMLLoader loader = new FXMLLoader();
        Parent rootNode = loader.load(getClass().getResourceAsStream(fxmlFile));

        Scene scene = new Scene(rootNode, 504 ,380);
        scene.getStylesheets().add("/css/FlatBee.css");
        stage.setTitle("Fact Extractor");
        stage.setScene(scene);
        stage.show();


        //  FXMLLoader loader = new FXMLLoader();
//        loader.setLocation(MainApp.class.getResource("view/MainApp.fxml"));

        // Даём контроллеру доступ к главному приложению.
        TextAnalyzerController controller = loader.getController();
        controller.setMainApp(this);
    }
}
