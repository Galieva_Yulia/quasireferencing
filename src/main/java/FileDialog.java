import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class FileDialog {

    static final FileChooser fileChooser = new FileChooser();;

    public static File chooseFile() {
        Stage stage=new Stage();
        configureFileChooser(fileChooser);
        File file = fileChooser.showOpenDialog(stage);
        return file;
    }

    public static void configureFileChooser(final FileChooser fileChooser){
        fileChooser.setTitle("Load file");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")+ "/Desktop"));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("txt", "*.txt")
        );
    }

}
