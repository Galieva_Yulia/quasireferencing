import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.*;


public class TextAnalyzerController {
    private MainApp mainApp;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    private TextAnalyzer textAnalizer;


    public TextAnalyzerController(){
        /*textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    textField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });*/

    }
    @FXML
    private TextArea taInfo;
    @FXML
    private TextArea taResult;
    @FXML
    private Button btnDo;
    @FXML
    private MenuItem miOpenFile;
    @FXML
    private MenuItem miCloseFile;
    @FXML
    private TextField textField;

    private String fullText = null;

    @FXML
    private void onClick_btnDo() throws Exception {
       // System.out.println("DoDODO!");
        textAnalizer = new TextAnalyzer();
        taResult.clear();
        if (fullText != null)
        textAnalizer.referText(fullText, Integer.parseInt(textField.getText()));
        taResult.setText(textAnalizer.getResult());
    }

    public void onClick_btnChooseTextFile(ActionEvent actionEvent) throws IOException {
        checkFileforParsing();
    }

    public void onClick_miClearTextFile(ActionEvent actionEvent) {
    }

    public void checkFileforParsing() throws IOException {
        File fileForParsing = FileDialog.chooseFile();
        if (fileForParsing != null) {
            fullText = readDataFromFile(fileForParsing);
            taInfo.setText(fullText);
        }
    }

    public static String readDataFromFile(File file) throws IOException {
        StringBuilder stringBuffer = new StringBuilder();
        BufferedReader bufferedReader = null;
        bufferedReader = new BufferedReader(new FileReader(file));
        String text;
        while ((text = bufferedReader.readLine()) != null)
        {
            stringBuffer.append(text);
            stringBuffer.append("\n");
        }

        return stringBuffer.toString();
    }



}
